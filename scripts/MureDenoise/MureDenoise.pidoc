%% ****************************************************************************
%% MureDenoise.pidoc - Released 2020/06/05 00:00:00 UTC
%% ****************************************************************************
%%
%% This file is part of MureDenoise Script Version 1.32
%%
%% Copyright (C) 2012-2020 Mike Schuster. All Rights Reserved.
%% Copyright (C) 2003-2020 Pleiades Astrophoto S.L. All Rights Reserved.
%%
%% Redistribution and use in both source and binary forms, with or without
%% modification, is permitted provided that the following conditions are met:
%%
%% 1. All redistributions of source code must retain the above copyright
%%    notice, this list of conditions and the following disclaimer.
%%
%% 2. All redistributions in binary form must reproduce the above copyright
%%    notice, this list of conditions and the following disclaimer in the
%%    documentation and/or other materials provided with the distribution.
%%
%% 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
%%    of their contributors, may be used to endorse or promote products derived
%%    from this software without specific prior written permission. For written
%%    permission, please contact info@pixinsight.com.
%%
%% 4. All products derived from this software, in any form whatsoever, must
%%    reproduce the following acknowledgment in the end-user documentation
%%    and/or other materials provided with the product:
%%
%%    "This product is based on software from the PixInsight project, developed
%%    by Pleiades Astrophoto and its contributors (http://pixinsight.com/)."
%%
%%    Alternatively, if that is where third-party acknowledgments normally
%%    appear, this acknowledgment must be reproduced in the product itself.
%%
%% THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
%% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
%% TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
%% PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
%% CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
%% EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
%% INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
%% DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%% POSSIBILITY OF SUCH DAMAGE.
%% ****************************************************************************

\documentclass PIScriptDoc

\script MureDenoise

\keywords {
   image denoising, mixed Poisson-Gaussian noise
}

\author {
   Mike Schuster
}

\copyright {
   2012-2020 Mike Schuster. All Rights Reserved.
}

\brief {
   Script for denoising linear monochannel images corrupted by mixed Poisson-Gaussian noise.
}

\description {
   MureDenoise denoises linear monochannel images corrupted by mixed Poisson-Gaussian noise. MureDenoise is applicable to single frame images and average combinations of equally exposed and registered frames.

   The script supports an astronomical image processing workflow in which the denoising step occurs immediately after the calibration and optional average combination steps and prior to other linear or nonlinear processing steps.

   The script applies a Haar-wavelet mixed noise unbiased risk estimator (MURE) to find a denoised output image that minimizes an estimate of the oracle mean-squared error (MSE) between the denoised output image and the unknown noise-free image.\ref{Stein_1981}\ref{Luisier_2010}

   \s{ Note }: For linear multichannel images from monocolor detectors, run the monochannel denoiser on each channel separately. The script does not work properly for images from one shot color (OSC) detectors.

   \s{ Warning }: The script is adapted to denoise linear monochannel images mainly corrupted by shot noise, read noise, and dark current noise which is typically the case for astronomical data. The script does not work properly for other noise distributions, for saturated images, for debayered images, for upsampled or downsampled images, for linearly or nonlinearly processed images, for median combinations, or for drizzle combinations.

   \s{ Warning }: Do not combine denoised images. Signal-to-noise ratio (SNR) will be enhanced by combining noisy images and denoising the result. Combined images must be equally exposed, have the same pixel resolution, and be registered by projective transformation with no distortion correction.

   This document describes MureDenoise Version 1.32.

   \subsection { Mixed Poisson-Gaussian noise hypothesis } {
      The mixed Poisson-Gaussian noise hypothesis used by MureDenoise is defined as y ~ &nu;&Rho;(&alpha;&eta;x/&nu;)/(&alpha;&eta;) + &Nu;(&delta;, &nu;&sigma;\sup{2}/&eta;), where

      \list {
         { y: noisy input image (DN), }
         { x: unknown noise-free image (DN), }
         { &alpha;: detector gain (e-/DN), }
         { &delta;: detector offset (DN), }
         { &sigma;: standard deviation of detector additive white Gaussian noise (DN), }
         { &eta;: combination count, and }
         { &nu;: variance scale. }
      }

      &nu;&Rho;(&alpha;&eta;x/&nu;)/(&alpha;&eta;) is a Poisson distribution with expectation x, variance &nu;x/(&alpha;&eta;), and identity covariance.

      &Nu;(&delta;, &nu;&sigma;\sup{2}/&eta;) is a Gaussian distribution with expectation &delta;, variance &nu;&sigma;\sup{2}/&eta;, and identity covariance.

      &eta; equals the combination count of the image or 1 for single frame images.

      Variance scale &nu; equals the product of a user specified \lref variance_scale {variance scale} parameter and the combination variance scaling function discussed in the \lref correlation_approximation {Image registration correlation approximation} section.

      The expectation &xi;\{y\} and variance &upsilon;\{y\} of the noisy input image y as a function of the unknown noise-free image x are given by

      \list {
         { &xi;\{y\} = x + &delta; and }
         { &upsilon;\{y\} = (&nu;/&eta;)(x/&alpha; + &sigma;\sup{2}). }
      }
   }

   \subsection { \label correlation_approximation {Image registration correlation approximation} } {
      The denoising method employs an approximation to partially compensate for the correlation that may be introduced by image registration. Image registration results in correlation between pixels within the interpolation filter's region of support which reduces the variance of a registered image. The correlation approximation models the Haar-wavelet correlation structure to improve output quality.

      Note that the Nearest Neighbor interpolation method introduces no correlation. The script provides maximum output quality when Nearest Neighbor is used.

      The first component of the correlation hypothesis is the variance scaling function S(&kappa;, &iota;) that defines the average scaling of variance at wavelet scale &kappa; that results from an application of interpolation method &iota;. Figures \fignum {variance_scaling_function} shows an estimate of S(&kappa;, &iota;) for the supported interpolation methods. Variance scaling values approach 1 at increasing scales due to the locality of the correlation. The variance scaling function was estimated by numerical integration over the unit pixel domain on a set of pseudorandom variates from the Gaussian distribution.

      \figure [numbered:variance_scaling_function, float:left, marginleft:36pt] {
         \figtag Variance scaling function S(&kappa;, &iota;)

         \image [margintop:6pt] Hypothesis/VarianceScalingFunction.png
      }

      \nf

      The second component of the correlation hypothesis is the combination variance scaling function C(&kappa;, &iota;, &eta;) that estimates the ratio of the normalized variance of the average combination of &eta; images registered with interpolation method &iota; to the normalized variance of an average combination of &eta; unregistered images at scale &kappa;. C(&kappa;, &iota;, &eta;) is defined as (1 + (&eta; - 1) S(&kappa;, &iota;)) / &eta;. The numerator represents the variance sum of one unregistered reference image with unit variance and &eta; - 1 registered images each with S(&kappa;, &iota;) variance. The denominator represents the variance sum of &eta; unregistered images each with unit variance.
   }

   \subsection { Image normalization and weighting approximations } {
      The normalized and weighted image combination performed by the \e{ImageIntegration} process can be reformulated as a simple sum of linearly transformed frames. The noise statistics of such a combination satisfies the Poisson-Gaussian noise hypothesis only in certain situations, such as for an unnormalized, equally weighted combination. As an approximation for the general situation, the denoising method defines two linear image transformations. The first, the combination transform, is defined as the coefficient-wise sum of the set of frame linear transforms. The second, the range transform, is defined as the normalizing transform used by \e{ImageIntegration} to bring its output within the nominal 0 to 1 value range. The denoising method applies the inverse of each of these transformations to the image to be denoised. The coefficients of these transformations, if non-identity, are logged in the process console.

      Note that unnormalized, equally weighted combinations require no approximation. The script provides maximum output quality when image normalization and weighting are disabled.
   }

   \subsection { Denoising method } {
      The denoising method is based on a statistical estimate of the oracle mean square error (MSE) between the unknown noise-free image and the denoised output image. Given a mixed Poisson-Gaussian noise hypothesis, the denoising method minimizes the MSE estimate over a set of denoising processes to find the optimal one, in the sense of peak-signal-to-noise ratio (PSNR).\ref{Stein_1981}\ref{Luisier_2010}

      The denoising processes are expressed as a linear combination of thresholding functions, from which only the weights are unknown. These weights are computed by a solution to a linear system of equations. This implies that all parameters of the method are determined automatically, without requiring user input.

      The denoising method applies MSE minimization independently at each scale of an unnormalized Haar-wavelet decomposition that preserves hypothesis noise statistics across scales. This independent MSE minimization at each wavelet scale is equivalent to a global image-domain MSE minimization, thanks to the orthogonality of Haar wavelets. The thresholding functions involve several parameters, which provides more adaptability than the standard single-parameter thresholding functions. In particular, the thresholds are adapted to local estimates of the signal-dependent noise variance, which are derived from the corresponding coarse coefficients at the same scale. The coarse coefficients are also used to incorporate interscale relationships into the thresholding functions.

      The denoising method uses cycle-spinning to suppress visual artifacts due to the lack of translation invariance of the decimated wavelet decomposition.\ref{Coifman_1995}\ref{Kamilov_2014} For a range of translations or shifts, cycle-spinning shifts the noisy image, denoises the shifted image, and then unshifts the denoised image. The result so produced are averaged together to form the output denoised image, which is nearly translation invariant.

      The denoising method provides an option to compensate for large scale noise variance scaling due to flatfielding. Large scale noise scaling variations, resulting from the flatfield correction of optical vignettes and detector sensitivity variations, are estimating by smoothing the flatfield. The denoising method compensates for these variations by multiplying the noisy image by the smoothed flatfield, denoising the product, and then dividing the result by the smoothed flatfield to form the output denoised image.
   }

   \subsection { Single frame image denoising } {
      Figure \fignum {single_frame_image_parameters} shows parameters for denoising a 40 minute single frame exposure of the ionized hydrogen region Sharpless Sh2-202 obtained with a Takahashi FSQ-106EDX 106 mm f/5 refractor, a monochrome Kodak KAF-8300 detector binned 2x2, and an Astrodon 3 nm H-alpha filter.

      The combination count parameter is set to 1 to specify that the image is a single frame exposure.

      The flatfield view is set to the image used for flatfield calibration to enable large scale flatfield compensation.

      Flatfield compensation is useful for telescopes with more than ~10\% optical vignetting. For telescopes with less vignetting, flatfield compensation results in negligible output quality improvement.

      The detector gain parameter is set to a value obtained from camera manufacture data provide by the EGAIN FITS file keyword. If detector gain is unknown, the \sref MureDenoiseDetectorSettings {MureDenoiseDetectorSettings} script can provide an estimate.

      The standard deviation of detector additive white Gaussian noise parameter is set to an estimate of the quadrature sum of read noise and dark current noise in a 40 minute dark provided by the \sref MureDenoiseDetectorSettings {MureDenoiseDetectorSettings} script. Detector offset is set to 0 because the image is dark-subtracted.

      The variance scale parameter is set to one to indicate that the nominal amount of denoising be performed. A value less than one will reduce the amount of denoising. A value greater than one will increase the amount of denoising.

      \s{ Warning }: Excessively large variance scale values risk the generation of denoising artifacts and the loss of signal-to-noise ratio (SNR). Denoising artifacts typically take on a “checkerboard” pattern, visible with high stretch in the background areas of the denoised image.

      The cycle-spin count parameter provides an adjustable tradeoff between output quality and processing time. The default cycle-spin count of 8 typically provides very good quality results in reasonable time. With this default, denoising a 4K x 4K pixel image requires several minutes of time on a late 2015 processor and a single-threaded PixInsight PJSR platform.

      \figure [numbered:single_frame_image_parameters, float:left, marginleft:36pt] {
         \figtag Single frame image parameters

         \image [margintop:6pt] Usage/2013_11_light_12_c_dialog.png
      }

      \nf

      Figure \fignum {single_frame_stack} shows a crop of the noisy and denoised single frame images with nonlinear stretch in a stack from which only one is visible at a time for efficient image comparison.

      \figure [numbered:single_frame_stack, float:left, marginleft:36pt] {
         \figtag Single frame noisy and denoised images

         \imageselect [margintop:6pt] {
            Images/2013_11_light_12_c_crop1.png
            { Noisy image }
            Images/2013_11_light_12_c_denoised_crop1.png
            { Denoised image }
         }
      }

      \nf
   }

   \subsection { Average combination image denoising } {
      Figure \fignum {average_combination_image_parameters} shows parameters for denoising an average combination of 8 registered frames of Sharpless Sh2-202, each a 40 minute exposure obtained in similar observing conditions with the same equipment.

      The combination count parameter is set to 8 to specify that the image is an 8 frame average combination.

      The interpolation method parameter is set to the Nearest Neighbor method that was used by the \e{StarAlignment} process to register the frames. Interpolation method must be set equal to the \e{StarAlignment} process parameter \e{Interpolation} > \e{Pixel interpolation}.

      The remaining parameters are identical to those shown in Figure \fignum {single_frame_image_parameters}.

      \figure [numbered:average_combination_image_parameters, float:left, marginleft:36pt] {
         \figtag Average combination image parameters

         \image [margintop:6pt] Usage/2013_11_light_integration_8_dialog.png
      }

      \nf

      Figures \fignum {average_combination_stack} and \fignum {average_combination_stack2} show a crop of the noisy and denoised average combination images with nonlinear stretch in a stack for efficient image comparison. The differences are hard to see in Figure \fignum {average_combination_stack} at actual size. Figure \fignum {average_combination_stack2} shows a crop of the same images at a 2x zoom where the differences are more apparent.

      \figure [numbered:average_combination_stack, float:left, marginleft:36pt] {
         \figtag Average combination noisy and denoised images

         \imageselect [margintop:6pt] {
            Images/2013_11_light_integration_8_crop1.png
            { Noisy image }
            Images/2013_11_light_integration_8_denoised_crop1.png
            { Denoised image }
         }
      }

      \nf

      \figure [numbered:average_combination_stack2, float:left, marginleft:36pt] {
         \figtag Average combination noisy and denoised images, 2x zoom

         \imageselect [margintop:6pt] {
            Images/2013_11_light_integration_8_crop2.png
            { Noisy image }
            Images/2013_11_light_integration_8_denoised_crop2.png
            { Denoised image }
         }
      }

      \nf
   }

   \subsection { Method noise } {
      MureDenoise provides an option to generate the method noise image. Method noise is the noise guessed by the denoising method, defined as the difference between the noisy input and the denoised output. Method noise should track hypothesis noise statistics, and is strongly signal dependent due to the presence of Poisson noise. The standard deviation of the method noise image is written to the process console as the \e {Method noise} value in DN units.

      Figure \fignum {method_noise_stack} shows crops of a denoised combination and the corresponding method noise image in a stack for efficient image comparison, the latter shown with linear stretch. Signal dependent method noise variations are clearly visible, however image structures unrelated to noise are small.

      \figure [numbered:method_noise_stack, float:left, marginleft:36pt] {
         \figtag Denoised image and method noise image

         \imageselect [margintop:6pt] {
            Images/2013_11_light_integration_8_denoised_crop1.png
            { Denoised image }
            Images/2013_11_light_integration_8_method_noise_crop1.png
            { Method noise image }
         }
      }

      \nf
   }
}

\usage {
   \subsection {MureDenoise} {
      \image [marginleft:36pt] Usage/2013_11_light_integration_8_dialog.png

      \division { Image } {
         \image [marginleft:36pt] Usage/image_group.png


         \definition {
            { View } {
               The view of the linear monochannel image selected for denoising. The image must be a single frame image or an average combination of similarly exposed and registered frames. The size of the image must be at least 256 pixels in width and height.

               \s{ Note }: For linear multichannel images from monocolor detectors, run the monochannel denoiser on each channel separately. The script does not work properly for images from one shot color (OSC) detectors.

               \s{ Warning }: The script is adapted to denoise linear monochannel images mainly corrupted by shot noise, read noise, and dark current noise which is typically the case for astronomical data. The script does not work properly for other noise distributions, for saturated images, for debayered images, for upsampled or downsampled images, for linearly or nonlinearly processed images, for median combinations, or for drizzle combinations.

               \s{ Warning }: Do not combine denoised images. Signal-to-noise ratio (SNR) will be enhanced by combining noisy images and denoising the result. Combined images must be equally exposed, have the same pixel resolution, and be registered by projective transformation with no distortion correction.
            }

            { Combination count } {
               The combination count of the image.

               \e{Combination count} must be set to 1 for single frame images.

               \e{Combination count} must be set to &eta; for average combinations of &eta; equally exposed and registered frames.
            }

            { Interpolation method } {
               The interpolation method used to register images for combination, as defined by the \e{StarAlignment} process.

               \e{Interpolation method} must be set equal to the \e{StarAlignment} process parameter \e{Interpolation} > \e{Pixel interpolation}. When \e{Auto} was selected as the \e{StarAlignment} pixel interpolation parameter, interpolation method must be set equal to the value recorded in the \e{StarAlignment} process log.

               The script supports the following interpolation methods:

               \list {
                  {
                     Nearest Neighbor
                  }

                  {
                     Bilinear
                  }

                  {
                     Bicubic Spline
                  }

                  {
                     Lanczos-3
                  }

                  {
                     Lanczos-4
                  }

                  {
                     Lanczos-5
                  }
               }

               The script provides maximum output quality when Nearest Neighbor is used.
            }
         }
      }

      \division { Flatfield } {
         \image [marginleft:36pt] Usage/flatfield_group.png

         \definition {
            { View } {
               To enable large scale flatfield compensation, the main view of the monochannel image used for flatfield calibration. The flatfield must be bias or dark-subtracted. To disable flatfield compensation, do not select a view.

               The sizes of the main view of the image selected for denoising and the main view of the flatfield must be equal.

               The standard deviation of the smoothed flatfield is written to the process console as the \e {Flatfield scale} value. The value is normalized as a percentage of the mean of the smoothed flatfield.
            }
         }
      }

      \division { Detector } {
         \image [marginleft:36pt] Usage/detector_group.png

         \definition {
            { Gain } {
               The gain of the detector in e-/DN.

               If detector gain is unknown, the \sref MureDenoiseDetectorSettings {MureDenoiseDetectorSettings} script can provide an estimate.

               Manufacture detector specifications may provide a gain value in e-/DN. This value may be used, with the risk that it may not correspond well to the actual detector configuration and performance. In general, the \sref MureDenoiseDetectorSettings {MureDenoiseDetectorSettings} script estimate is more reliable.
            }

            { Gaussian noise } {
               The standard deviation of Gaussian noise of the detector in DN.

               If detector Gaussian noise is unknown, the \sref MureDenoiseDetectorSettings {MureDenoiseDetectorSettings} script can provide an estimate.

               Manufacture detector specifications may provide a read noise value in e-. This value, when divided by detector gain to obtain a value in DN, may be used, with the risk that it may not correspond well to the actual detector configuration and performance. In general, the \sref MureDenoiseDetectorSettings {MureDenoiseDetectorSettings} script estimate is more reliable.
            }

            { Offset } {
               The offset of the detector in DN.

               If detector offset is unknown, the \sref MureDenoiseDetectorSettings {MureDenoiseDetectorSettings} script can provide an estimate.

               Detector offset should be set to zero for denoising calibrated (i.e., bias or dark-subtracted) images. Detector offset should be set to the bias or dark frame median for denoising uncalibrated (i.e., neither bias nor dark-subtracted) images.
            }
         }
      }

      \division { Denoise } {
         \image [marginleft:36pt] Usage/denoise_group.png

         \definition {
            { \label variance_scale {Variance scale} } {
               This parameter scales hypothesis noise variance. A value of one corresponds to nominal hypothesis noise variance and a corresponding nominal amount of denoising. A value less than one will reduce the amount of denoising. A value greater than one will increase the amount of denoising.

               \s{ Warning }: Excessively large variance scale values risk the generation of denoising artifacts and the loss of signal-to-noise ratio (SNR). Denoising artifacts typically take on a “checkerboard” pattern, visible with high stretch in the background areas of the denoised image.
            }

            { Cycle-spin count } {
               \e{Cycle-spin count} provides an adjustable trade-off between output quality and processing time. Increasing the number of cycle-spins improves denoising quality, but also increases (nearly linearly) processing time.

               The script works at multiple resolutions. To create the coarser resolutions, the script combines pixels from finer resolutions. There are multiple ways to choose which pixels to combine. Each cycle-spin chooses different pixels to combine, and performs a complete denoising operation. The results from all of the cycle-spins are averaged together to produced the final result. The net effect is to average out the variations in noise estimation due to pixel choice, and so gives a better result.

               The default cycle-spin count of 8 typically provides very good quality results in reasonable time.
            }

            { Use image metadata } {
               Use \e{ImageIntegration} generated image metadata to compensate for the process's image normalization and weighting operations.

               The following \e{ImageIntegration} settings are supported: \e{Image integration} > \e{Combination}: \e{Average}. \e{Image integration} > \e{Normalization}: \e{No normalization} or \e{Additive with scaling}. \e{Image integration} > \e{Weights}: all settings.

               The script provides maximum output quality when image normalization and weighting are disabled.
            }

            { Include gradient classifier } {
               Include a gradient classifier that exploits local gradient squared magnitude to local noise variance ratio relationships to provide an increase in the adaptivity and accuracy of the denoising process, but also increases processing time by about 50\%.
            }

            { Generate method noise image } {
               Generate the method noise of the denoising process as a new image window. Method noise is the noise guessed by the denoising method, defined as the difference between the noisy input and the denoised output. Method noise should track hypothesis noise statistics, and is strongly signal dependent due to the presence of Poisson noise.

               The standard deviation of the method noise image is written to the process console as the \e {Method noise} value in DN units.
            }
         }
      }
   }

   \subsection { Button pane } {
      \image [marginleft:36pt] Usage/button_pane.png

      \definition {
         { \image Usage/new_instance.png } {
            Create a new instance.
         }

         { \image Usage/browse_documentation.png  } {
            Open a browser to view documentation.
         }

         { \image Usage/reset.png } {
            Reset all parameters.
         }

         { Denoise } {
            Denoise the image.
         }

         { Dismiss } {
            Dismiss the dialog or abort the denoising.
         }

      }
   }

}

\section { Limitations and known issues } {
   \s{ Warning }: The script is adapted to denoise linear monochannel images mainly corrupted by shot noise, read noise, and dark current noise which is typically the case for astronomical data. The script does not work properly for other noise distributions, for saturated images, for debayered images, for upsampled or downsampled images, for linearly or nonlinearly processed images, for median combinations, or for drizzle combinations.

   \s{ Warning }: Do not combine denoised images. Signal-to-noise ratio (SNR) will be enhanced by combining noisy images and denoising the result. Combined images must be equally exposed, have the same pixel resolution, and be registered by projective transformation with no distortion correction.

   \s{ Warning }: Excessively large Denoise Variance scale values risk the generation of denoising artifacts and the loss of signal-to-noise ratio (SNR). Underestimates of detector gain, overestimates of detector Gaussian noise, and underestimates of detector offset risk similar problems. Denoising artifacts typically take on a “checkerboard” pattern, visible with high stretch in the background areas of the denoised image.
}

\relatedscripts {
   MureDenoiseDetectorSettings
}

\reference Coifman_1995 {
   R. Coifman and D. Donoho, “Translation invariant de-noising”, in \e { Lecture Notes in Statistics: Wavelets and Statistics }, 103:125–150, Springer Verlag, New York, 1995.
}

\reference Kamilov_2014 {
   U.S. Kamilov, E. Bostan, and M. Unser, "Variational Justification of Cycle Spinning for Wavelet-Based Solutions of Inverse Problems", \e {IEEE Signal Processing Letters}, 21(11):1326-1330, November 2014.
}

\reference Luisier_2010 {
   F. Luisier, "The SURE-LET Approach to Image Denoising", École polytechnique fédérale de Lausanne, Thèse N° 4566, January 2010.
}

\reference Stein_1981 {
   C. Stein, "Estimation of the Mean of a Multivariate Normal Distribution", \e { The Annals of Statistics }, 9(6):1135-1151, November 1981.
}

\make

%% ****************************************************************************
%% MureDenoise.pidoc - Released 2020/06/05 00:00:00 UTC
