%% ****************************************************************************
%% MureDenoiseDetectorSettings.pidoc - Released 2020/05/08 00:00:00 UTC
%% ****************************************************************************
%%
%% This file is part of MureDenoiseDetectorSettings Script Version 1.3
%%
%% Copyright (C) 2012-2020 Mike Schuster. All Rights Reserved.
%% Copyright (C) 2003-2020 Pleiades Astrophoto S.L. All Rights Reserved.
%%
%% Redistribution and use in both source and binary forms, with or without
%% modification, is permitted provided that the following conditions are met:
%%
%% 1. All redistributions of source code must retain the above copyright
%%    notice, this list of conditions and the following disclaimer.
%%
%% 2. All redistributions in binary form must reproduce the above copyright
%%    notice, this list of conditions and the following disclaimer in the
%%    documentation and/or other materials provided with the distribution.
%%
%% 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
%%    of their contributors, may be used to endorse or promote products derived
%%    from this software without specific prior written permission. For written
%%    permission, please contact info@pixinsight.com.
%%
%% 4. All products derived from this software, in any form whatsoever, must
%%    reproduce the following acknowledgment in the end-user documentation
%%    and/or other materials provided with the product:
%%
%%    "This product is based on software from the PixInsight project, developed
%%    by Pleiades Astrophoto and its contributors (http://pixinsight.com/)."
%%
%%    Alternatively, if that is where third-party acknowledgments normally
%%    appear, this acknowledgment must be reproduced in the product itself.
%%
%% THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
%% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
%% TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
%% PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
%% CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
%% EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
%% INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
%% DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%% POSSIBILITY OF SUCH DAMAGE.
%% ****************************************************************************

\documentclass PIScriptDoc

\script MureDenoiseDetectorSettings

\keywords {
   MureDenoise, detector settings, image denoising
}

\author {
   Mike Schuster
}

\copyright {
   2012-2020 Mike Schuster. All Rights Reserved.
}

\brief {
   Script for estimating detector settings for use by the \sref MureDenoise {MureDenoise} script.
}

\description {
   MureDenoiseDetectorSettings estimates detector settings for use by the \sref MureDenoise {MureDenoise} script.

   The script requires as input two uncalibrated flat frames and two bias or two dark frames. The use of two dark frames rather than two bias frames allows the script to account for dark current noise.

   The medians of the two uncalibrated flat frames should be equal within 10\%. The medians of the two bias or two dark frames should be equal within 10\%.

   The script provides values for detector gain, detector Gaussian noise, and detector offset for use by the \sref MureDenoise {MureDenoise} script.

   The script also provides a measure of the median flat frame exposure, in the form of the median of the pixel-wise mean of the center cropped bias- or dark-subtracted flat frames. To provide sufficient signal-to-noise ratio and to avoid near saturation nonlinearity, this measure should be between roughly 30\% and 70\% of detector full-well.

   This document describes MureDenoiseDetectorSettings Version 1.3.

   \subsection { Algorithm } {
      \division { Detector gain } {
         Compute the bias- or dark-subtracted flat frames. Compute the pixel-wise sum and pixel-wise difference between the center cropped bias- or dark-subtracted flat frames. Frames are cropped to the central 50\% frame area. Compute the median of the local means of the sum. Compute the median of the local standard deviations of the difference. Detector gain equals the normalized ratio between the median local mean and the square of the median local standard deviation.
      }

      \division { Detector Gaussian noise } {
         Compute the pixel-wise difference between the bias or dark frames. Compute the median of the local standard deviations of the difference. Detector Gaussian noise equals the normalized median local standard deviation.
      }

      \division { Detector offset } {
         Zero or bias or dark frame median. Detector offset should be set to zero for denoising calibrated (i.e., bias or dark-subtracted) images. Detector offset should be set to the bias or dark frame median for denoising uncalibrated (i.e., neither bias nor dark-subtracted) images.
      }
   }
}

\usage {
   \subsection {MureDenoiseDetectorSettings} {
      \image [marginleft:36pt] Usage/dialog.png

      \division { Calibration frames } {
         \image [marginleft:36pt] Usage/calibration_frames_group.png

         \definition {
            {Uncalibrated flat frame 1} {
               Uncalibrated flat frame 1. The medians of the two uncalibrated flat frames should be equal within 10\%.
            }

            {Uncalibrated flat frame 2} {
               Uncalibrated flat frame 2. The medians of the two uncalibrated flat frames should be equal within 10\%.
            }

            {Bias or dark frame 1} {
               Bias or dark frame 1. The medians of the two bias or two dark frames should be equal within 10\%.
            }

            {Bias or dark frame 2} {
               Bias or dark frame 2. The medians of the two bias or two dark frames should be equal within 10\%.
            }

            {Flat frame exposure} {
               A measure of the median flat frame exposure, in the form of the median of the pixel-wise mean of the center cropped bias- or dark-subtracted flat frames. To provide sufficient signal-to-noise ratio and to avoid near saturation nonlinearity, this measure should be between roughly 30\% and 70\% of detector full-well.
            }
         }
      }

      \division { Detector settings } {
         \image [marginleft:36pt] Usage/detector_settings_group.png

         \definition {
            {Gain} {
               Estimated detector gain.
            }

            {Gaussian noise} {
               Estimated detector Gaussian noise.
            }

            {Offset} {
               Estimated detector offset. Detector offset should be set to zero for denoising calibrated (i.e., bias or dark-subtracted) images. Detector offset should be set to the bias or dark frame median for denoising uncalibrated (i.e., neither bias nor dark-subtracted) images.
            }

            {Offset Mode} {
               Detector offset mode. Select \e{Zero offset} for denoising calibrated (i.e., bias or dark-subtracted) images. Select \e{Bias or dark frame median offset} for denoising uncalibrated (i.e., neither bias nor dark-subtracted) images.
            }
         }
      }

      \division { Button pane } {
         \image [marginleft:36pt] Usage/button_pane.png

         \definition {
            { \image Usage/new_instance.png } {
               Create a new instance.
            }

            { \image Usage/browse_documentation.png  } {
               Open a browser to view documentation.
            }

            { \image Usage/reset.png } {
               Reset all parameters.
            }

            { Estimate } {
               Estimate detector settings.
            }

            { Dismiss } {
               Dismiss the dialog or abort the estimation.
            }
         }
      }
   }
}

\relatedscripts {
   MureDenoise
}

\make

%% ****************************************************************************
%% MureDenoise.pidoc - Released 2020/05/08 00:00:00 UTC
